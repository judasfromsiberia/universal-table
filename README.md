# table

Реализована универсальная таблица
	
![Alt Text](https://media.giphy.com/media/j5zm7ZrgparRceZIVO/giphy.gif)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
