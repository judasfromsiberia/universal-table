import axios from "axios";

async function getData() {
  const url =
    "http://www.filltext.com/?rows=1000&id=%7Bindex%7D&fullname=%7BfirstName%7D~%7BlastName%7D&company=%7Bbusiness%7D&email=%7Bemail%7D&uname=%7Busername%7D&address=%7BaddressObject%7D";
  return axios
    .get(url)
    .then(function(response) {
      //handle success
      return response;
    })
    .catch(function(error) {
      //handle error
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    });
}

export { getData };
