import Vue from "vue";
import Vuex from "vuex";
import { getData } from "@/api/getData.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {
    async getData() {
      const data = await getData();
      return data;
    }
  },
  modules: {}
});
